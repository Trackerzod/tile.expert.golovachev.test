<?php
namespace App\Services\HtmlImportService;

use App\Services\FileService\Interfaces\FileService;
use App\Services\HtmlParserService\HtmlParserService;
use App\Services\ImageDownloaderService\ImageDownloaderService;
use App\Services\ImageProcessorService\ImageProcessorService;
use finfo;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class HtmlImportService
{
    /**
     * @var HtmlParserService
     */
    private $htmlParserService;

    /**
     * @var ImageDownloaderService
     */
    private $imageDownloaderService;

    /**
     * @var Client
     */
    private $httpClient;

    /**
     * @var FileService
     */
    private $fileService;

    /**
     * @var ImageProcessorService
     */
    private $imageProcessor;

    public function __construct(
        HtmlParserService $htmlParserService, 
        ImageDownloaderService $imageDownloaderService, 
        FileService $fileService,
        ImageProcessorService $imageProcessor
        )
    {
        $this->htmlParserService = $htmlParserService;
        $this->imageDownloaderService = $imageDownloaderService;
        $this->httpClient = new Client();
        $this->fileService = $fileService;
        $this->imageProcessor = $imageProcessor;
    }

    public function importImagesFromPage(string $pageUrl, int $minWidth=0, int $minHeight=0)
    {
        // Вытаскивем теги картинок из страницы
        $html = $this->loadPageContent($pageUrl);
        $this->htmlParserService->load($html);
        $imageTags = $this->htmlParserService->findTag('img');

        // перебираем теги
        $paths = [];
        foreach($imageTags as $imageTag) {
            $imageContent = null;
            // вытаскиваем ссылки из тегов и скачиваем
            $path = $this->htmlParserService->getTagAttributeValue($imageTag, 'src');
            if ($this->urlContainHost($path)) {
                $imageContent = $this->imageDownloaderService->downloadImage($path);
            } else {
                $host = $this->extractHost($pageUrl);
                $imageContent = $this->imageDownloaderService->downloadImage($host.$path);
            }

            if ($this->isImage($this->getMimeTypeFromContent($imageContent))) {
                $imageInfo = $this->imageProcessor->getImageInfoByString($imageContent);
                // если размеры файла удовлетворяют минимальным размерам то сохраняем файл
                if ($imageInfo['width'] >= $minWidth && $imageInfo['height'] >= $minHeight) {
                    // Уменьшаем картинку до 200px по высоте и обрезаем до 200px по ширине
                    $imageContent = $this->imageProcessor->scaleByHeightFromString($imageContent, 200);
                    $imageContent = $this->imageProcessor->cropByWidthFromString($imageContent, 200);

                    // Генерируем путь и имя файла и сохраняем его
                    $downloadedPath = $this->generatePath();
                    $fileName = Uuid::uuid4()->toString().$this->getExtensionFromContent($imageContent);
                    if ($this->fileService->put($downloadedPath.'/'.$fileName, $imageContent)) {
                        $paths[] = $this->fileService->getHtmlPubilcPath().'/'.$downloadedPath.'/'.$fileName;
                    }
                }
            }
            
        }
        return $paths;
    }

    public function extractHost(string $url)
    {
        $parsedUrl = parse_url($url);
        if ($this->urlContainHost($url)) {
            $host = $parsedUrl['scheme'].'://'.$parsedUrl['host'];
            return  $host;
        }
        return false;
    }

    public function urlContainHost(string $url) {
        $parsedUrl = parse_url($url);
        return array_key_exists('host', $parsedUrl) 
               && array_key_exists('scheme', $parsedUrl);
    }

    public function getMimeTypeFromContent(string $content)
    {
        $finfo = new finfo(FILEINFO_MIME_TYPE);
        return $finfo->buffer($content);
    }

    public function isImage(string $mime) {
        return $mime === 'image/png'  ||
               $mime === 'image/jpeg' ||
               $mime === 'image/jpg'  ||
               $mime === 'image/gif';
    }

    public function getExtensionFromContent(string $content)
    {
        $mime = $this->getMimeTypeFromContent($content);
        switch ($mime) {
            case 'image/png':
                return '.png';
            case 'image/jpeg':
                return '.jpg';
            case 'image/jpg':
                return '.jpg';
            case 'image/gif':
                return '.gif';
            default:
                return null;
        }
    } 

    /**
     * Генерирует путь к файлу относительно текущей даты, относительно корневой папки с файлами.
     *
     * @return string
     */
    private function generatePath() {
        $date = date('Y/m/d');
        return (string)$date;
    }

    private function loadPageContent(string $pageUrl) {
        try {
            $response = $this->httpClient->request('GET', $pageUrl);
            $html = $response->getBody()->getContents();
            return $html;
        } catch(GuzzleException $e) {
            throw new NotFoundHttpException('Page not found from given URL');
        }
        
    }
}