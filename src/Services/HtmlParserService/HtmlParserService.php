<?php
namespace App\Services\HtmlParserService;

use PHPHtmlParser\Dom;
use PHPHtmlParser\Dom\HtmlNode;

class HtmlParserService
{
    /**
     * @var Dom
     */
    private $htmlPage;

    public function __construct(Dom $htmlPage)
    {
        $this->htmlPage = $htmlPage;
    }

    /**
     * Загрузка страницы, парсинг возвращение сервиса для дальнейшей работы
     *
     * @param string $html
     * @return HtmlParserService
     */
    public function load(string $html)
    {
        $this->htmlPage->load($html);
        return $this;
    }

    public function findTag(string $tag)
    {
        $tags = $this->htmlPage->find($tag);
        return $tags->toArray();
    }

    public function getTagAttributeValue(HtmlNode $tagObject, string $attribute)
    {
        $attributeValue = $tagObject->getAttribute($attribute);
        return $attributeValue;
    }
}