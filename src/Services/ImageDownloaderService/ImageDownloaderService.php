<?php
namespace App\Services\ImageDownloaderService;

use GuzzleHttp\Client;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ImageDownloaderService
{
    /**
     * @var Client
     */
    private $httpClient;

    public function __construct()
    {
        $this->httpClient = new Client();
    }

    public function downloadImage($url) {
        $image = $this->apiClient('GET', $url);
        return $image;
    }

    private function apiClient(string $method, string $url, array $options=[])
    {
        try {
            $response = $this->httpClient->request($method, $url, $options);
            if ($response->getStatusCode() >= 400) {
                return false;
            }
            return $response->getBody()->getContents();
        } catch(\Exception $e) {
            return false;
        }
    }
}