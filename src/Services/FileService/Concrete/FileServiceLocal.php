<?php
namespace App\Services\FileService\Concrete;

use App\Services\FileService\Interfaces\FileService;
use Symfony\Component\Filesystem\Filesystem;

class FileServiceLocal implements FileService
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var string
     */
    private $rootPath;

    /**
     * @var string
     */
    private $htmlPublicPath;

    public function __construct(Filesystem $filesystem, string $rootPath, string $htmlPublicPath)
    {
        $this->filesystem = $filesystem;
        $this->rootPath = $rootPath;
        $this->htmlPublicPath = $htmlPublicPath;
    }

    /**
     * @return string
     */
    public function getRootPath() {
        return $this->rootPath;
    }

    /**
     * @return string
     */
    public function getHtmlPubilcPath()
    {
        return $this->htmlPublicPath;
    }

    /**
     * Запись строки в файл
     *
     * @param string $filePath
     * @param string $content
     * @return bool
     */
    public function put(string $filePath, string $content)
    {
        $fullFilePath = $this->rootPath.'/'.$filePath;
        $this->filesystem->dumpFile($fullFilePath, $content);
        return $this->exists($filePath);
    }

    /**
     * Проверка существования файла
     *
     * @param string $path
     * @return bool
     */
    public function exists(string $path)
    {
        $fullFilePath = $this->rootPath.'/'.$path;
        return $this->filesystem->exists($fullFilePath);
    }
}