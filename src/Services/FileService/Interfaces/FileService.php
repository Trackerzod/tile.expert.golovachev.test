<?php
namespace App\Services\FileService\Interfaces;

interface FileService
{
    /**
     * @return string
     */
    public function getRootPath();

    /**
     * @return string
     */
    public function getHtmlPubilcPath();

    /**
     * Запись строки в файл
     *
     * @param string $filePath
     * @param string $content
     * @return bool
     */
    public function put(string $filePath, string $content);

    /**
     * Проверка существования файла
     *
     * @param string $path
     * @return bool
     */
    public function exists(string $path);
}