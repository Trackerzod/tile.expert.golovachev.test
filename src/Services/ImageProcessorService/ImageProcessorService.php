<?php
namespace App\Services\ImageProcessorService;

use App\Services\FileService\Interfaces\FileService;

class ImageProcessorService
{
    public function getImageInfoByFile(string $filePath)
    {
        $info = getimagesize($filePath);
        if ($info) {
            $result = [];
            $result['width'] = $info[0];
            $result['height'] = $info[1];
            $result['ext'] = $this->detectExtension($info[2]);
            $result['mime'] = $info['mime'];
            $result['bits'] = $info['bits'];
            $result['channels'] = $info['channels'];

            return $result;
        }
    }

    public function getImageInfoByString(string $fileContent)
    {
        $info = getimagesizefromstring($fileContent);
        if ($info) {
            $result = [];
            $result['width'] = $info[0];
            $result['height'] = $info[1];
            $result['ext'] = $this->detectExtension($info[2]);
            $result['mime'] = $info['mime'];

            return $result;
        }
    }

    public function scaleByWidthFromString(string $imageContent, int $width)
    {
        $image = imagecreatefromstring($imageContent);
        $fileInfo = $this->getImageInfoByString($imageContent);
        $image = imagescale($image, $width);
        // FIXME: пофиксить вывод буфера, буфер не должен выводиться
        $scaledImageString = $this->extractImageContent($image, $fileInfo);

        return $scaledImageString;
    }

    public function scaleByHeightFromString(string $imageContent, int $height)
    {
        $image = imagecreatefromstring($imageContent);
        $fileInfo = $this->getImageInfoByString($imageContent);
        $image = imagescale($image, $fileInfo['width'], $height);
        $scaledImageString = $this->extractImageContent($image, $fileInfo);

        return $scaledImageString;
    }

    public function cropByWidthFromString($imageContent, int $width)
    {
        $image = imagecreatefromstring($imageContent);
        $fileInfo = $this->getImageInfoByString($imageContent);
        $rect = [
            'x' => $fileInfo['width'] / 2 - $width / 2,
            'y' => 0,
            'width' => $width,
            'height' => $fileInfo['height']
        ];
        $image = imagecrop($image, $rect);
        $cropedImageString = $this->extractImageContent($image, $fileInfo);

        return $cropedImageString;
    }

    public function cropByHeightFromString(string $imageContent, int $height)
    {
        $image = imagecreatefromstring($imageContent);
        $fileInfo = $this->getImageInfoByString($imageContent);
        $rect = [
            'x' => 0,
            'y' => $fileInfo['height'] / 2 - $height / 2,
            'width' => $fileInfo['width'],
            'height' => $height
        ];
        $image = imagecrop($image, $rect);
        $cropedImageString = $this->extractImageContent($image, $fileInfo);

        return $cropedImageString;
    }

    private function extractImageContent($image, array $fileInfo)
    {
        $stream = fopen('php://memory', 'w+');
        ($this->gdImage($fileInfo['ext'])($image, $stream));
        rewind($stream);
        $imageContent = stream_get_contents($stream);

        return $imageContent;
    }

    private function detectExtension(int $gdImageType)
    {
        switch ($gdImageType) {
            case IMAGETYPE_GIF:
                return 'gif';
            case IMAGETYPE_BMP:
                return 'bmp';
            case IMAGETYPE_ICO:
                return 'ico';
            case IMAGETYPE_IFF:
                return 'iff';
            case IMAGETYPE_JPEG:
                return 'jpg';
            case IMAGETYPE_JPEG2000:
                return 'jpg';
            case IMAGETYPE_PNG:
                return 'png';
            case IMAGETYPE_TIFF_II:
                return 'tiff';
            case IMAGETYPE_TIFF_MM:
                return 'tiff';
            default:
                return 'unknown';
        }
    }

    private function gdImage(string $extension)
    {
        switch ($extension) {
            case 'gif':
                return 'imagegif';
            case 'bmp':
                return 'imagebmp';
            case 'png':
                return 'imagepng';
            default:
                return 'imagejpeg';
        }
    }
}