# Тестовое задание PHP-программиста
## Требования
* php-7.3
* php-gd
* php-ctype
* php-iconv
* php-json
* php-pcre
* php-session
* php-xml
* php-tokenizer
* composer
* symfony (https://symfony.com/download)
* nodejs >=v11.10

## Как развернуть
Перед развертывание следует выполнить в консоли команду `symfony check:requirements
` и выполнить требования если таковые появятся
* `git clone git@bitbucket.org:Trackerzod/tile.expert.golovachev.test.git`
* `composer install`
* `npm install`
* `npm run build` для однократной компиляции ассетов или `npm run dev` для компиляции после каждого изменения ассетов
* `php ./bin/console server:start`