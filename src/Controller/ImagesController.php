<?php

namespace App\Controller;

use App\Form\ImageDownloaderType;
use App\Services\HtmlImportService\HtmlImportService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/", name="images_")
 */
class ImagesController extends AbstractController
{
    /**
     * @var HtmlImportService
     */
    private $htmlImportService;

    public function __construct(HtmlImportService $htmlImportService)
    {
        $this->htmlImportService = $htmlImportService;
    }

    /**
     * @Route("/", name="index")
     */
    public function index(SessionInterface $session)
    {
        $form = $this->createForm(ImageDownloaderType::class, null, 
            [
                'action' => $this->generateUrl('images_download_from_url'),
                'method' => 'POST',
            ]
        );
        $imagesPaths = $session->get('tile_expert_golovachev_app', []);
        return $this->render('images/index.html.twig', [
            'controller_name' => 'ImagesController',
            'form' => $form->createView(),
            'imagesPaths' => $imagesPaths
        ]);
    }

    /**
     * @Route("download", name="download_from_url")
     */
    public function downloadFromUrl(Request $request, SessionInterface $session)
    {
        $form = $this->createForm(ImageDownloaderType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $imagePaths = $this->htmlImportService->importImagesFromPage(
                $data['url'],
                $data['min_width'],
                $data['min_height']
            );
            $session->set('tile_expert_golovachev_app', $imagePaths);
            return $this->json([
                'paths' => $imagePaths
            ]);
        }

        return $this->json([
            'error' => 'Something wrong'
        ], 500);
    }
}
