const path = require('path');

module.exports = {
  entry: './assets/js/app.js',
  output: {
    filename: 'index.js',
    path: path.resolve(__dirname, 'public/assets/js'),
  },
  module: {
    rules: [
        {
            test: /\.css$/i,
            use: [
               'style-loader',
               'css-loader'
            ]
        },
        {
            test: /\.scss$/,
            use: [
                'style-loader',
                'css-loader',
                'sass-loader'
            ]
        }
    ]
  }
};