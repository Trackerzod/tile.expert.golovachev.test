<?php

namespace App\Tests\Services;

use App\Services\FileService\Concrete\FileServiceLocal;
use App\Services\ImageProcessorService\ImageProcessorService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Filesystem\Filesystem;

class ImageProcessorTest extends TestCase
{
    public function testGetImageInfoByFile()
    {
        $rootPath = \dirname(\dirname(__DIR__));
        $imageProcessorService = new ImageProcessorService();
        $imagePath = $rootPath.'/public/test/0b51c21c-9a05-47e5-b9f9-2ee1c29c0028.jpg';
        $result = $imageProcessorService->getImageInfoByFile($imagePath);
        $this->assertIsArray($result);
        $this->assertArrayHasKey('width', $result);
        $this->assertArrayHasKey('height', $result);
        $this->assertArrayHasKey('ext', $result);
        $this->assertArrayHasKey('mime', $result);
    }

    public function testGetImageInfoByString()
    {
        $rootPath = \dirname(\dirname(__DIR__));
        $imageProcessorService = new ImageProcessorService();
        $imagePath = $rootPath.'/public/test/0b51c21c-9a05-47e5-b9f9-2ee1c29c0028.jpg';
        $fileContent = file_get_contents($imagePath);
        $result = $imageProcessorService->getImageInfoByString($fileContent);
        $this->assertIsArray($result);
        $this->assertArrayHasKey('width', $result);
        $this->assertArrayHasKey('height', $result);
        $this->assertArrayHasKey('ext', $result);
        $this->assertArrayHasKey('mime', $result);
    }

    public function testCropByWidthFromString()
    {
        $rootPath = \dirname(\dirname(__DIR__));
        $imageProcessorService = new ImageProcessorService();
        $imagePath = $rootPath.'/public/test/0b51c21c-9a05-47e5-b9f9-2ee1c29c0028.jpg';
        $fileContent = file_get_contents($imagePath);
        $result = $imageProcessorService->cropByWidthFromString($fileContent, 30);
        $this->assertIsString($result);
        // file_put_contents($rootPath.'/public/test/0b51c21c-9a05-47e5-b9f9-2ee1c29c0028-crop.jpg', $result);
    }

    public function testCropByHeightFromString()
    {
        $rootPath = \dirname(\dirname(__DIR__));
        $imageProcessorService = new ImageProcessorService();
        $imagePath = $rootPath.'/public/test/0b51c21c-9a05-47e5-b9f9-2ee1c29c0028.jpg';
        $fileContent = file_get_contents($imagePath);
        $result = $imageProcessorService->cropByHeightFromString($fileContent, 30);
        $this->assertIsString($result);
        file_put_contents($rootPath.'/public/test/0b51c21c-9a05-47e5-b9f9-2ee1c29c0028-crop.jpg', $result);
    }

    public function testScaleByWidthFromString()
    {
        $rootPath = \dirname(\dirname(__DIR__));
        $imageProcessorService = new ImageProcessorService();
        $imagePath = $rootPath.'/public/test/0b51c21c-9a05-47e5-b9f9-2ee1c29c0028.jpg';
        $fileContent = file_get_contents($imagePath);
        $result = $imageProcessorService->scaleByWidthFromString($fileContent, 100);
        $this->assertIsString($result);
        // file_put_contents($rootPath.'/public/test/0b51c21c-9a05-47e5-b9f9-2ee1c29c0028-scale.jpg', $result);
    }

    public function testScaleByHeightFromString()
    {
        $rootPath = \dirname(\dirname(__DIR__));
        $imageProcessorService = new ImageProcessorService();
        $imagePath = $rootPath.'/public/test/0b51c21c-9a05-47e5-b9f9-2ee1c29c0028.jpg';
        $fileContent = file_get_contents($imagePath);
        $result = $imageProcessorService->scaleByHeightFromString($fileContent, 100);
        $this->assertIsString($result);
        // file_put_contents($rootPath.'/public/test/0b51c21c-9a05-47e5-b9f9-2ee1c29c0028-scale.jpg', $result);
    }
}
