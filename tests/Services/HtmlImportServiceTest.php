<?php

namespace App\Tests\Services;

use App\Services\FileService\Concrete\FileServiceLocal;
use App\Services\HtmlImportService\HtmlImportService;
use App\Services\HtmlParserService\HtmlParserService;
use App\Services\ImageDownloaderService\ImageDownloaderService;
use App\Services\ImageProcessorService\ImageProcessorService;
use GuzzleHttp\Client;
use PHPHtmlParser\Dom;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Filesystem\Filesystem;

class HtmlImportServiceTest extends TestCase
{
    public function testExtractHost()
    {
        $htmlImportService = new HtmlImportService(
            new HtmlParserService(new Dom),
            new ImageDownloaderService(new Client()),
            new FileServiceLocal(new Filesystem(), '/home/alex/dev/php/tile.expert.golovachev/public/uploads', '/uploads'),
            new ImageProcessorService()
        );
        $url = 'https://animemotivation.com/cute-anime-girls/';
        $domain = $htmlImportService->extractHost($url);
        $this->assertIsString($domain);
    }

    public function testImportImagesFromPage()
    {
        $htmlImportService = new HtmlImportService(
            new HtmlParserService(new Dom),
            new ImageDownloaderService(new Client()),
            new FileServiceLocal(new Filesystem(), '/home/alex/dev/php/tile.expert.golovachev/public/uploads', '/uploads'),
            new ImageProcessorService()
        );
        $url = 'https://animemotivation.com/cute-anime-girls/';
        $res = $htmlImportService->importImagesFromPage($url, 400, 400);
        $this->assertIsArray($res);
    }
}
