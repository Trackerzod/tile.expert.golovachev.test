<?php

namespace App\Tests;

use App\Services\ImageDownloaderService\ImageDownloaderService;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;

class ImageDownloaderServiceTest extends TestCase
{
    public function testDownloadImage()
    {
        $imageDownloaderService = new ImageDownloaderService();
        $url = 'https://venturebeat.com/wp-content/uploads/2015/06/windwaker.jpg?resize=1276%2C929&strip=all';
        $imageContent = $imageDownloaderService->downloadImage($url);
        $this->assertIsString($imageContent);
    }
}
