<?php

namespace App\TestsServices;

use App\Services\HtmlParserService\HtmlParserService;
use PHPHtmlParser\Dom;
use PHPUnit\Framework\TestCase;

class HtmlParserServiceTest extends TestCase
{
    public function testLoad()
    {
        $htmlParserService = new HtmlParserService(new Dom);
        $html = "
            <div>test message</div>
        ";
        $sameHtmlParserService = $htmlParserService->load($html);
        $this->assertInstanceOf(HtmlParserService::class, $sameHtmlParserService);
    }

    public function testFindTag()
    {
        $htmlParserService = new HtmlParserService(new Dom);
        $html = "
            <body>
                <div>test message</div>
                <a>Link</a>
            </body>
        ";
        $htmlParserService->load($html);
        $tags = $htmlParserService->findTag('a');
        $this->assertIsArray($tags);
        $this->assertTrue(count($tags) === 1);
    }

    public function testGetTagAttributeValue()
    {
        $htmlParserService = new HtmlParserService(new Dom);
        $html = "
            <body>
                <div>test message</div>
                <a href=\"http://example.com\">Link</a>
            </body>
        ";
        $htmlParserService->load($html);
        $tags = $htmlParserService->findTag('a');
        $this->assertIsArray($tags);
        $this->assertTrue(count($tags) === 1);
        $attributeValue = $htmlParserService->getTagAttributeValue($tags[0], 'href');
        $this->assertIsString($attributeValue);
        $this->assertTrue($attributeValue === 'http://example.com');
    }
}
