<?php

namespace App\Tests;

use App\Services\FileService\Concrete\FileServiceLocal;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Filesystem\Filesystem;

class FileServiceLocalTest extends TestCase
{
    public function testPut()
    {
        $rootPath = \dirname(\dirname(__DIR__));
        $filesystem = new Filesystem();
        $fileService = new FileServiceLocal($filesystem, $rootPath.'/public/uploads');
        $content = '123';
        $fileIsWrited = $fileService->put('2019/10/11/text.txt', $content);
        $this->assertTrue($fileIsWrited);
    }

    public function testExists()
    {
        $rootPath = \dirname(\dirname(__DIR__));
        $filesystem = new Filesystem();
        $fileService = new FileServiceLocal($filesystem, $rootPath.'/public/uploads');
        //$folderCreated = mkdir($rootPath.'/2019/10/11', 0664, true);
        //$this->assertTrue($folderCreated);
        $bytes = file_put_contents($rootPath.'/2019/10/11/text1.txt', '321');
        $this->assertTrue($bytes);
        $this->assertTrue($fileService->exists($rootPath.'/2019/10/12/text.txt'));
    }
}
