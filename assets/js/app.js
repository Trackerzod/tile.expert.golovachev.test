import '../css/main.scss';
import $ from 'jquery';

$(document).ready(function() {
    $('#image-downloader-form').submit(function(e) {
        e.preventDefault();

        var form = $(this);
        var url = form.attr('action');
        var submitButton = $(form.find('#image_downloader_submit'));
        disableButton(submitButton, 'Downloading. Hold on...');

        $.ajax({
            type: 'POST',
            url: url,
            data: form.serialize(),
            success: function(data) {
                var imagesBlock = $('.pictures-content');
                renderImages(data.paths, imagesBlock);
                enableButton(submitButton, 'Submit');
            }
        });
        
    });
});

function disableButton(jqueryButton, message) {
    jqueryButton.prop('disabled', true);
    jqueryButton.text(message);
}

function enableButton(jqueryButton, message) {
    jqueryButton.prop('disabled', false);
    jqueryButton.text(message);
}

function renderImages(pathsArray, blockWrapperJquery) {
    pathsArray.forEach(element => {
        var imgTag = $('<img>');
        imgTag.attr('src', element);
        imgTag.appendTo(blockWrapperJquery);
    });
}